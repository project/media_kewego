<?php

/**
 *  @file
 *  Create a Kewego Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $kewego = new ResourceKewegoStreamWrapper('kewego://?v=[video-code]');
 */
class MediaKewegoStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://kewego.fr/video/';

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/kewego';
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    $Kewego = new KewegoConnection;
    $xml_videos_datas = $Kewego->get_videos_thumbnail(check_plain($parts['v']));
    return $xml_videos_datas;
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-kewego/' . check_plain($parts['v']) . '.jpg';
    //$local_path = 'public://media-kewego/video.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}
