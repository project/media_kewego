<?php

/**
 * @file
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_kewego_file_formatter_info() {
  $formatters['media_kewego_video'] = array(
    'label' => t('Kewego Video'),
    'file types' => array('video'),
    'default settings' => array(
      'width' => media_kewego_variable_get('width'),
      'height' => media_kewego_variable_get('height'),
      'autoplay' => media_kewego_variable_get('autoplay'),
    ),
    'view callback' => 'media_kewego_file_formatter_video_view',
    'settings callback' => 'media_kewego_file_formatter_video_settings',
  );
  $formatters['media_kewego_image'] = array(
    'label' => t('Kewego Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_kewego_file_formatter_image_view',
    'settings callback' => 'media_kewego_file_formatter_image_settings',
  );
  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_kewego_file_formatter_video_view($file, $display, $langcode) {
  
  $scheme = file_uri_scheme($file->uri);
  
  // check if media_derivatives_kewego is installed and load derivative uri if exist
  if (module_exists('media_derivatives_kewego')) {
    if ($file->media_derivatives['has_derivatives'] 
      && (isset($file->media_derivatives['derivatives_list']['kewego_video']))) {
      // check managed derivative
      if ($file->media_derivatives['derivatives_list']['kewego_video']->derivative_fid) {
        $derivative = file_load($file->media_derivatives['derivatives_list']['kewego_video']->derivative_fid);
        $der_uri = $derivative->uri;
      }
      // check unmanaged derivative
      elseif ($file->media_derivatives['derivatives_list']['kewego_video']->unmanaged_uri != null) {
        $der_uri = $file->media_derivatives['derivatives_list']['kewego_video']->unmanaged_uri;
      }
    }
  }
  
  // WYSIWYG does not yet support video inside a running editor instance.
  if (($scheme == 'kewego' || (isset($der_uri))) 
    && empty($file->override['wysiwyg'])) {
    if (isset($der_uri)) $uri = $der_uri;
    else $uri = $file->uri;
    $element = array(
      '#theme' => 'media_kewego_video',
      '#uri' => $uri,
    );
    foreach (array('width', 'height', 'autoplay') as $setting) {
      $element['#' . $setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_kewego_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );
  $element['autoplay'] = array(
    '#title' => t('Autoplay'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autoplay'],
  );
  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_kewego_file_formatter_image_view($file, $display, $langcode) {

  $scheme = file_uri_scheme($file->uri);
  
  // check if media_derivatives_kewego is installed and load kewego thumbnail if exist
  if (module_exists('media_derivatives_kewego')) {
    if ($file->media_derivatives['has_derivatives'] 
      && (isset($file->media_derivatives['derivatives_list']['kewego_video']))) {
      // check managed derivative
      if ($file->media_derivatives['derivatives_list']['kewego_video']->derivative_fid) {
        $derivative = file_load($file->media_derivatives['derivatives_list']['kewego_video']->derivative_fid);
        $der_uri = $derivative->uri;
      }
      // check unmanaged derivative
      elseif ($file->media_derivatives['derivatives_list']['kewego_video']->unmanaged_uri != null) {
        $der_uri = $file->media_derivatives['derivatives_list']['kewego_video']->unmanaged_uri;
      }
    }
  }
  
  if ($scheme == 'kewego' || isset($der_uri)) {
    if (isset($der_uri)) $uri = $der_uri;
    else $uri = $file->uri;
    $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);
    if (empty($image_style) 
      || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => $wrapper->getOriginalThumbnailPath(),
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
      );
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_kewego_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );
  return $element;
}

/**
 * Implements hook_file_default_displays().
 */
function media_kewego_file_default_displays() {
  $default_displays = array();

  // Default settings for displaying as a video.
  $default_video_settings = array(
    'media_large' => array(
      'width' => 640,
      'height' => 360,
      'autoplay' => FALSE,
    ),
    'media_original' => array(
      'width' => 640,
      'height' => 360,
      'autoplay' => media_kewego_variable_get('autoplay'),
    ),
  );
  foreach ($default_video_settings as $view_mode => $settings) {
    $display_name = 'video__' . $view_mode . '__media_kewego_video';
    $default_displays[$display_name] = (object) array(
      'api_version' => 1,
      'name' => $display_name,
      'status' => 1,
      'weight' => 1,
      'settings' => $settings,
    );
  }

  // Default settings for displaying a video preview image. We enable preview
  // images even for view modes that also play video, for use inside a running
  // WYSIWYG editor. The higher weight ensures that the video display is used
  // where possible.
  $default_image_styles = array(
    'media_preview' => 'square_thumbnail',
    'media_large' => 'large',
    'media_original' => ''
  );
  foreach ($default_image_styles as $view_mode => $image_style) {
    $display_name = 'video__' . $view_mode . '__media_kewego_image';
    $default_displays[$display_name] = (object) array(
      'api_version' => 1,
      'name' => $display_name,
      'status' => 1,
      'weight' => 2,
      'settings' => array('image_style' => $image_style),
    );
  }

  return $default_displays;
}
