<?php

/**
 * Menu callback; presents the Media editing form.
 */
function media_page_kewego_thumbnail($file) {
  
  return drupal_get_form('media_kewego_thumbnail', $file);
}

/**
 * Form builder: Builds the edit file form.
 */
function media_kewego_thumbnail($form, $form_state, $file) {

  $preview = media_get_thumbnail_preview($file);
  
  $form['container'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Thumbnail'),
  );
  $form['container']['thumbnail'] = array(
        '#type' => 'markup',
        '#markup' => drupal_render($preview),
      );
  $form['#submit'] = array (
    '#type'             => 'submit',
    '#value'            => t('Submit Me')
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#weight' => 5,
    '#submit' => array('media_kewego_thumbnail_submit'),
  );
  
  return $form;

}

/**
 * Attempt to delete files and notify the user of the result.
 */
function media_kewego_thumbnail_submit($form, &$form_state) {
  $kewego_uri = $form_state['build_info']['args'][0]->media_derivatives['derivatives_list']['kewego_video']->unmanaged_uri;
  $wrapper = file_stream_wrapper_get_instance_by_uri($kewego_uri);
  $parts = $wrapper->get_parameters();
  $sig = check_plain($parts['v']);
  
  
  $thumbnail_uri = 'public://media-kewego/'.$sig.'.jpg';
  drupal_unlink($thumbnail_uri, $context = NULL);
  image_path_flush($thumbnail_uri);
  
  watchdog('Kewego Thumbnail', 'Thumbnail filename: <PRE>%s</PRE>', array('%s' => print_r($thumbnail_uri, true)), WATCHDOG_INFO);
}