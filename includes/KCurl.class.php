<?php

set_time_limit(0);

class KCurl {
  function Call($api_url, $data_array = '', $post = true) {
    if (substr($api_url, strlen($api_url)-1, 1) != '/') {
      $api_url = $api_url.'/';
    }

    if (extension_loaded('curl')) {
      //watchdog('Curl', 'Calling API method: '.$api_url.' '.htmlentities(implode('&',$data_array)), array(), WATCHDOG_INFO);
      $parsed_url = parse_url($api_url);
      $ch         = curl_init($parsed_url['scheme'].'://'.$parsed_url['host'].$parsed_url['path']);

      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
      curl_setopt($ch, CURLOPT_VERBOSE, true);
      curl_setopt($ch, CURLOPT_FAILONERROR, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      if ($post === true ) curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
      curl_setopt($ch, CURLOPT_TIMEOUT, 100);

      $post_data = array();
      if (isset($parsed_url['query'])) {
        $query_arg = explode('&',$parsed_url['query']);
        foreach($query_arg as $key => $arg) {
          $detail_arg = explode('=', $arg);
          $post_data[$detail_arg[0]] = $detail_arg[1];
        }
      }

      if ($data_array != '') {
        foreach($data_array as $data_key => $data) {
          $post_data[$data_key] = $data;
        }
      }
			
      curl_setopt($ch, CURLOPT_POSTFIELDS, substr(KCurl::data_encode($post_data), 0, -1));

      $xml	= curl_exec($ch);

      $error = curl_error($ch);
      curl_close($ch);

      if (!empty($error)) {
        watchdog('Curl', $error, array(), WATCHDOG_INFO);
      }
      return $xml;
    }
    else {
      watchdog('Curl', 'You need the CURL extension', array(), WATCHDOG_INFO);
    }
  }


  function getMessage() {
    if ($response = $this->getResponse()) {
      $xml_message = simplexml_load_string($response);
      $message = $xml_message->message;
      watchdog('Curl', 'Response: '.(string)$message, array(), WATCHDOG_INFO);
      return (string)$message;
    }
    else {
      return false;
      watchdog('Curl', 'No response', array(), WATCHDOG_INFO);
    }
  }


  function createXMLData($datas) {
    $xml = new XMLWriter();
    $xml->openMemory();
    $xml->setIndentString("\t");
    $xml->setIndent(true);
    $xml->startDocument('1.0', 'utf-8');

    $xml->startElement('kewego_call');
    $xml->startElement('params');
    
    foreach ($datas as $key => $value) {
      $xml->startElement($key);
      if (is_array($value)) {
        foreach($value as $subkey => $subvalue) {
          if (is_numeric($subkey)) {
            foreach($subvalue as $balise => $balise_value) {
              $xml->startElement($balise);
              $xml->writeRaw($balise_value);
              $xml->endElement();
            }
          }
          else {
            $xml->startElement($subkey);
            $xml->writeRaw($subvalue);
            $xml->endElement();
          }
        }
      }
      else {
        $xml->writeRaw($value);
      }
      $xml->endElement();
    }
    $xml->endElement();
    $xml->endElement();
    return $xml->outputMemory(false);
  }

  function data_encode($data, $keyprefix = "", $keypostfix = "") {
    assert( is_array($data) );
    $vars=null;
    foreach($data as $key=>$value) {
      if(is_array($value)) {
        $vars .= data_encode($value, $keyprefix.$key.$keypostfix.urlencode("["), urlencode("]"));
      }
      else {
        $vars .= $keyprefix.$key.$keypostfix."=".urlencode($value)."&";
      }
    }
    return $vars;
  }
}

function APICall() {
  static $app;

  if (!is_object($app)) {
    $app = new KCurl;
  }
  return $app;
}

?>