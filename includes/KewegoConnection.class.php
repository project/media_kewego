<?php

require_once('KCurl.class.php');

class KewegoConnection
{
  
  public $appKey = '';
  public $appToken = '';
  public $authToken = '';
  public $username = '';
  public $password = '';
  
  function __construct()
  {
  
    $this->appKey = variable_get('media_kewego_ApiKey', '');
    $this->username = variable_get('media_kewego_username', '');
    $this->password = variable_get('media_kewego_password', '');
    
    /*
    Checking and Getting appToken
    */
    
    if (isset($_SESSION['appToken'])) {
      // there is a session variable  
      $this->appToken = $_SESSION['appToken'];
      
	  //Checking previous appToken validity
	  
	  if ($this->check_app_token() === false) {
	    // appToken is expired
	    if ($this->appToken = $this->get_app_token()) {
	      // new appToken stored in session
          $_SESSION['appToken'] = $this->appToken;
        }
        else {
          watchdog('Kewego Connection', 'Unable to get appToken', array(), WATCHDOG_INFO);
        }
      }
    }
    else {
      // there is no session variable  
	  if ($this->appToken = $this->get_app_token()) {
	    // new appToken stored in session
        $_SESSION['appToken'] = $this->appToken;
	  }
  	  else {
        watchdog('Kewego Connection', 'Unable to get appToken', array(), WATCHDOG_INFO);
	  }
    }

    /*
    Checking and Getting authToken
    */
    
    if (isset($_SESSION['authToken'])) {
      // there is a session variable
      $this->authToken = $_SESSION['authToken'];
      
	  if ($this->check_auth_token() === false) {
	    if ($this->authToken = $this->get_auth_token()) {
	      // new authToken stored in session
          $_SESSION['authToken'] = $this->get_auth_token();
	    }
      }
    }
    else {
      // there is no session variable  
	  if ($this->authToken = $this->get_auth_token()) {
	    // new authToken stored in session
        $_SESSION['authToken'] = $this->authToken;
      }
	  else {
        watchdog('Kewego Connection', 'Unable to get authToken', array(), WATCHDOG_INFO);
	  }
    }
    
  }
  
  function get_app_token() {
	$response = APICall()->Call('http://api.kewego.com/app/getToken/', array('appKey' => $this->appKey));
	$xml_message = simplexml_load_string($response);
	$appToken = $xml_message->message->appToken;
	//watchdog('Kewego', '--> appToken is: '.$appToken, array(), WATCHDOG_INFO);
	return (string)$appToken;
  }
  
  function check_app_token() {
	$response = APICall()->Call('http://api.kewego.com/app/checkToken/', array('appToken' => $this->appToken));
	$xml_message = simplexml_load_string($response);
	$validity = $xml_message->message->appToken;

	if ($validity == true) {
      //watchdog('Kewego', '--> appToken is still valid', array(), WATCHDOG_INFO);
      return true;
	}
	return false;
  }
  
  function get_auth_token() {
	$response = APICall()->Call('http://login.kewego.com/api/getAuthToken/', array('username' => $this->username.'@kewego.com', 'password' => $this->password, 'appToken' => $this->appToken));
	$xml_message = simplexml_load_string($response);
	$authToken = $xml_message->message->token;
	//watchdog('Kewego', 'authToken is: '.$authToken, array(), WATCHDOG_INFO);
	return (string)$authToken;
  }

  function check_auth_token() {
	$response = APICall()->Call('http://api.kewego.com/auth/checkToken/', array('token' => $this->authToken, 'appToken' => $this->appToken));
	$xml_message = simplexml_load_string($response);
	$validity = $xml_message->message->check_token;
	if ( "true" == $validity) {
      //watchdog('Kewego', '--> authToken is still valid', array(), WATCHDOG_INFO);
      return true;
	}
	return false;
  }
  
  function get_videos_detail($sig) {
	$response = trim(APICall()->Call('http://api.kewego.com/video/getDetails/', array( 'sig' => $sig, 'appToken' => $this->appToken)));
	$xml_message = simplexml_load_string($response,'SimpleXMLElement',LIBXML_NOCDATA);
	$Video_details = $xml_message->message;
	return $Video_details;
  }
  
  function get_videos_list($start = 0, $limit = 10) {
	$response = trim(APICall()->Call('http://api.kewego.com/user/getVideos/', array( 'username' => $this->username, 'appToken' => $this->appToken, 'token' => $this->authToken, 'start' => $start, 'limit' => $limit)));
	$xml_message = simplexml_load_string($response,'SimpleXMLElement',LIBXML_NOCDATA);
	$Video_list = $xml_message->message;
	return $Video_list;
  }
  
  function get_videos_thumbnail($sig = NULL, $number = '1', $size = 'large') {
	$response = trim(APICall()->Call('http://api.kewego.com/video/getThumbnailsURL/', array( 'sig' => $sig, 'appToken' => $this->appToken,'number' => $number, 'size' => $size)));
    $xml_message = simplexml_load_string($response,'SimpleXMLElement',LIBXML_NOCDATA | LIBXML_NOBLANKS);
    $Video_Thumbnail = $xml_message->message->thumbnails->image;
	return $Video_Thumbnail;
  }
  
  function get_player_code($sig = NULL, $autostart = true, $width = null, $height = null, $language_code = 'fr', $version = 'epix') {
	$response = trim(APICall()->Call('http://api.kewego.com/video/getPlayerCode/', array( 'sig' => $sig, 'appToken' => $this->appToken, 'autostart' => $autostart, 'width' => $width, 'height' => $height, 'language_code' => $language_code, 'version' => $version)));
    $xml_message = simplexml_load_string($response,'SimpleXMLElement',LIBXML_NOCDATA);
    $Player_code = $xml_message->message->blog;
	return $Player_code;
  }
  
  //upload a video
  function upload_video($file) {
    $sig = $file->filename;
    $filepath = drupal_realpath($file->uri);
    $progressID = md5_file($filepath);
    $url = 'http://upload.kewego.com/api/uploadVideo/index.php?X-Progress-ID='.$progressID;
  
    $apptoken = $this->appToken;
    $authtoken = $this->authToken;
  
    $xml_data = $this->generate_xml_details($file);
  
    watchdog('Kewego', 'XML generated for upload : '.htmlentities($xml_data), array(), WATCHDOG_INFO);

    $data = array(
      'file'         => '@'.$filepath,
      'xml_data'     => $xml_data,
      'token'       => $authtoken,
      'appToken'    => $apptoken
    );
					
    $ch  = curl_init($url);
    
    // setting curl options and datas for upload
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
  
    if(( $response = curl_exec($ch)) === false) {
      watchdog('Kewego', 'Erreur Curl : '.curl_error($ch), array(), WATCHDOG_INFO);
      return false;
	}
	else {
	  $parsed_response = simplexml_load_string($response);
	  $new_sig = $parsed_response->message->sig;
	  watchdog('Kewego', 'Video has been correctly sent to API. API returned : <br/>'.$response.'<br/>'.$new_sig, array(), WATCHDOG_INFO);
	  return $new_sig;
	}
	
	curl_close($ch);
  }
  
  function generate_xml_details($file) {
  
    if (isset($file->media_title[LANGUAGE_NONE])) {
      $file_title =  check_plain($file->media_title[LANGUAGE_NONE][0]['value']);
    } else {
      $file_title =  check_plain($file->filename);
    }
  
    if (isset($file->media_description[LANGUAGE_NONE][0]['value'])) {
      $file_description =  $file->media_description[LANGUAGE_NONE][0]['value'];
    } else {
      $file_description =  'uploaded from events.eurocopter.com';
    }
  
    // display all keywords
    $keywords = '';
  
    if (isset($file->field_tags[LANGUAGE_NONE]) && count($file->field_tags[LANGUAGE_NONE])) {
      foreach($$file->field_tags[LANGUAGE_NONE] as $tid){
        $term = taxonomy_term_load($tid['tid']);
        $keywords .= '<keyword>'.$term->name.'</keyword>';
      }
    } else {
      $keywords .= '<keyword>Events</keyword><keyword>Eurocopter</keyword>';
    }
  
    $xml_data = "<kewego_call>
					<params>
						<title>$file_title</title>
					    <description>$file_description</description>
					    <categories>
					      <category>Aviation</category>
					    </categories>
					    <keywords>
					      $keywords
					    </keywords>
					    <language>fr</language>
					    <country>fr</country>
					    <postal_code>13000</postal_code>
					    <access>public</access>
					    <commentable>no</commentable>
					    <shareable>no</shareable>
					    <notable>no</notable>
					    <production_date>2011-01-01 12:00:00</production_date>
					</params>
			  </kewego_call>";
    return $xml_data;
  }
  
}
  
?>