<?php

/**
 * @file
 * Administration settings for media_kewego button integration
 */

/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 */
function media_kewego_admin_settings() {

   
  $form = array();

  $form['media_kewego_general_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['media_kewego_general_settings']['media_kewego_username'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Username'),
    '#default_value' => variable_get('media_kewego_username', 'my-username'),
    '#description'   => t('Your username for Kewego access. Example: my-username'),
  );
  $form['media_kewego_general_settings']['media_kewego_password'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Password'),
    '#default_value' => variable_get('media_kewego_password', 'my-password'),
    '#description'   => t('Your password for Kewego access. Example: my-password'),
  );
  $form['media_kewego_general_settings']['media_kewego_ApiKey'] = array(
    '#type'          => 'textfield',
    '#title'         => t('API Key'),
    '#default_value' => variable_get('media_kewego_ApiKey', 'key'),
    '#description'   => t('Your API Key for Kewego access.'),
  );
  $form['media_kewego_general_settings']['	media_kewego_playerKey'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Player Key'),
    '#default_value' => variable_get('media_kewego_playerKey', 'Player Key'),
    '#description'   => t('The Kewego player key.'),
  );
  $form['test'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Test token'),
  );
  
  $Kewego = new KewegoConnection;
  
  $appToken = '';
  $authtoken = '';
  $markup_appToken = 'Apptoken is '.$Kewego->appToken.'<br>';
  $markup_authtoken = 'Authtoken is '.$Kewego->authToken.'<br>';
  
  $form['test']['appToken'] = array(
    '#type' => 'markup',
    '#markup' => $markup_appToken,
  );
  $form['test']['authtoken'] = array(
    '#type' => 'markup',
    '#markup' => $markup_authtoken,
  );
  
  
  return system_settings_form($form);
}